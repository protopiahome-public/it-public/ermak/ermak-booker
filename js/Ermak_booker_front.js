var show_hubs_cont 		= function(){},
	show_gt_cont		= function(){},
	show_factory_cont	= function(){},
	circle_menu_position= function(){},
	mobilbasiccontresize= function(){},
	eb_chng_fctgt		= function(){},
	get_slide_id		= function(){};
	var circle_move, coffset, cavatarc;
jQuery( document ).ready(function( $ ) 
{  
	$(".ardn_button").live({click:function(evt)
	{
		var clid		= $(this).attr("clid");
		var hhhh		= $("#fc_over"+clid).height();
		$(".eb_fc_over").height(0);
		$(".eb_fc_over").children().css({"opacity":0});
		if(hhhh < 5)
		{
			$("#fc_over"+clid).height($("#fc_over"+clid).children().height() + 8);
			$("#fc_over"+clid).children().css({"opacity":1});
		}
	}});
	$(".take_gb_button").live({click:function(evt)
	{
		var slide_id	= jssor_slider3.$CurrentIndex();
		var inset_id	= $(this).parents("[inset_id]").attr("inset_id");
		var acc			= -1;
		var count		= -1;
		send(['eb_pay_gb', $(this).attr("gbid"), acc, count, slide_id, inset_id], false);
		var wid			= $(this).width();
		$(this).empty().append( get_wait_form(0, "padding:0!important; margin:0!important; width:" + wid + "px; height:20px;font-size:20px; color:#EEE;") );
		//$("[slide_id=" + slide_id + "]").empty().append(get_wait_form(0, "color:#FFF;"));
	}});
	//
	
	$("[storgbid]").live({change:function(evt)
	{
		var storgbid	= ($(this).attr("storgbid"));
		var slide_id	= jssor_slider3.$CurrentIndex();
		var inset_id	= $(this).parents("[inset_id]").attr("inset_id");
		send(["eb_change_store", storgbid, $(this).attr("checked"), slide_id, inset_id], false);
	}});
	$(".eb_refresh").live({click:function(evt)
	{
		var slide_id	= $(this).parents(".eb_slider_container").attr("slide_id"); //jssor_slider3.$CurrentIndex();
		var inset_id	= $(this).parents("[inset_id]").attr("inset_id");
		send(['eb_refresh_mobile', slide_id, inset_id], false);
		$("[slide_id=" + slide_id + "]").empty().append(get_wait_form(0, "color:#FFF;"));
	}});
	
	
	//CYRCLE
	$(".eb_cbutton").live({
		click:function(evt)
		{
			var exec	= $(this).attr("exec");
			if(exec)	send([exec], false);
			$(this).children("img").hide();
			$(this).append(get_wait_form(1));
			$(this).children(".smc_wait_spin").css({'padding':'7px 0px', 'margin':0, 'width': '46px', 'height': '46px', "color":"#333"});
		}	
	});
	
	$("#avacenter").live({
		mousedown:function(evt)
		{
			//alert("torch");
			circle_move = true;
			var off		= $(this).offset();
			coffset		= {top:evt.pageY - off.top, left:evt.pageX - off.left};
			cavatarc	= $(this).parent();
		}
	});	
	$("body").mousemove(function(evt)
	{	
		if(circle_move){
			$(cavatarc).offset({left:(evt.pageX - coffset.left), top:(evt.pageY - coffset.top)});
		}
	});
	$("body").mouseup(function(evt)
	{	
		if(circle_move)
		{
			var offset	= $(cavatarc).offset();
			send(["move_circle_menu", offset.top-30, offset.left], false);
		}
		circle_move = false;
		cavatarc = null;
	});
	
	//switch's buttons on window title
	show_hubs_cont = function(params)
	{
		var p	= params.split(',');
		send(['show_hubs_cont', p], false);
	}
	show_gt_cont = function(params)
	{
		var p	= params.split(',');
		send(['show_gt_cont', p], false);
	}
	show_factory_cont = function(params)
	{
		var p	= params.split(',');
		send(['show_factory_cont', p], false);
	};	
	//
	
	$('html').click(function() 
	{
		$('.top_submenu').hide();
		//eb_res = false;
	});
	$(".tsmenuel").live({click:function(e)
	{
		//a_alert("exec_send=" + $(this).parent().attr("exec_send") + ", id="+$(this).attr("id"));
		send([$(this).parent().attr("exec_send"), $(this).attr("id")], false);
	}});
	$(".tmenuelem").live({click:function(event)
	{
		event.stopPropagation();
		var exec		= $(this).attr("exec");
		if(exec)
			send([exec],false);
		//$(this).siblings('.top_submenu').fadeIn('slow');
	}});
	$("#add_win").live({click: function(e)
	{
		send(['eb_add_window'], false);
	}});
	$(".eb_float_title_cont .smc_switch_button ").live({
		click:function(evt)
		{
			var cont	= $(this).parent().parent().parent().parent().parent();
			cont.children(".eb_float_cont").empty().append(get_wait_form(1));
			var id		= (cont.attr('id'));
		}
	});
	$(window).resize(function() 
	{
		circle_menu_position();
		mobilbasiccontresize();
	})
	mobilbasiccontresize = function()
	{
		//return;
		var height			= $(window).height();
		if($(".eb_mobcont").size()==0)	return;
		//$(".eb_mobcont").height(height-140);
		//switcher contents resized for window sizes
		var swid			= $(".eb_slider_container:eq(" + jssor_slider3.$CurrentIndex() + ") .smc_switcher_body").parent().attr("id");
		switch(swid)
		{
			case "ermak_news":
				var sw		= "#" + swid + " .smc_switcher_body";
				var h		= $(window).height() - $(sw).offset().top;
				$(sw + " .smc_switch_slide").each(function(i, elem)
				{
					$(elem).height(h-55);
					$(elem).width($(window).width()-20);
					$(elem).addClass("bb_container");
					//$(elem).css({border:"1px dotted red"});
				});
				break;
		}	
	}
	circle_menu_position = function()
	{
		if($("#eb_cyrcle_menu").size() == 0) return;
		var left		= $("#eb_cyrcle_menu").attr("left");
		if(width-220 > left)
		{
			$("#eb_cyrcle_menu").offset({left:left});
		}
		else if(width-220 < left)
		{
			$("#eb_cyrcle_menu").offset({left:width-220});
		}
	}
	//change factory settings
	$("[arg2=eb_chnge_fpr]>.smcnumdn, [arg2=eb_chnge_fpr]>.smcnumup").live({
		click:function(evt)
		{
			var pr	= $(this).parent().children('input[type="hidden"]').val();
			var iid	= $(this).parent().attr("arg1");
			var slide_id	= jssor_slider3.$CurrentIndex();
			//a_alert(pr + ", " + iid + ", slide_id="+slide_id);
			send(['eb_chng_fct_prdct', iid, pr, slide_id], true);
		}
	});
	eb_chng_fctgt = function(args)
	{
		var aa			= args.split(",");
		var slide_id	= jssor_slider3.$CurrentIndex();
		send(['eb_chng_fctgt', aa[0], aa[1], slide_id]);
	}
	
	//----------------
	get_slide_id = function()
	{
		var slideIndex			= jssor_slider3.$CurrentIndex();
		var slide_id			= jQuery(".eb_slider_container:eq("+ slideIndex + ")").attr("slide_id");
		return slide_id;
	}
});	

	document.documentElement.addEventListener("post_draw", function(e)
	{
		if(jQuery("#eb_cyrcle_menu").size() == 0) return;
		if(jQuery("#eb_cyrcle_menu").position().left+220 > width)
		{
			jQuery("#eb_cyrcle_menu").offset({left:width-220});
		}
	}); 
document.documentElement.addEventListener("_send_", function(e) 
{	
	( function( $ )
	{
		var dat			= e.detail;
		var command		= dat[0];
		var datas		= dat[1];
		//console.log(command);
		switch(command)
		{
			case 'eb_add_window':
				//console.log(datas);
				$("body").append(datas['new_win']);
				break;
			case "show_mine_gt":
			case "show_factories":
			case "show_hubs":
			case "eb_show_bank":
			case "eb_show_tools":
			case "eb_show_locations":
				$("body").append(datas['text']);
				$(".eb_cbutton img").fadeIn('slow');
				$(".eb_cbutton #smc_wait_spin").detach();
				//add_and_open_clapan3("", datas['text'][0], (datas['text'][1]));
				break;
			case 'show_hubs_cont':
				$("[slug=" + datas['cont'] + "] .eb_float_cont").empty().append($(datas['text']).hide().fadeIn("slow"));
				break;
			case 'show_gt_cont':
				$("[slug=" + datas['cont'] + "] .eb_float_cont").empty().append($(datas['text']).hide().fadeIn("slow"));
				break;
			case 'show_factory_cont':
				$("[slug=" + datas['cont'] + "] .eb_float_cont").empty().append($(datas['text']).hide().fadeIn("slow"));
				break;
			case "eb_refresh_mobile":
			case "eb_pay_gb":
			case "eb_change_store":
				$("[slide_id="+datas['slide_id'] + "]").empty().css({"pointer-events" : "auto"}).append($(datas['text']).children());
				if($("[slide_id="+datas['slide_id'] + "]").attr("eb_exec"))
				{
					var exec	= $("[slide_id="+datas['slide_id'] + "]").attr("eb_exec");
					var args	= $("[slide_id="+datas['slide_id'] + "]").attr("eb_args");
					if(exec)	window[exec](args, datas['data']);
				}
				resized();
				mobilbasiccontresize();
				break;
			case 'eb_chng_fct_prdct':
			case 'eb_chng_fctgt':
				//a_alert(datas['text']);
				$("[.eb_slider_container][slide_id="+datas['slide_id'] +"]").empty().append(datas['text']);
				resized();
				mobilbasiccontresize();
				break;
			case 'pay_gb':
				var slide_id		= get_slide_id();
				send(['eb_refresh_mobile', slide_id], false);
				jQuery("[slide_id=" + slideIndex + "] .eb_refresh").find(".fa-refresh").addClass("fa-spin");			
				break;
		}
	})(jQuery);
});