<?php 
	
	
	class Ermak_Float_Widget extends WP_Widget
	{
		static $data;
		/**
		 * Constructor
		 *
		 * @return void
		 * @since 0.5.0
		 */
		function Ermak_Float_Widget()
		{
			parent::__construct( false, __('Ermak Float Booker Widget', ERMAK_BOOKER), array('description' => __("Floating widget generator for Ermak Players comfort", ERMAK_BOOKER), 'classname' => 'ermak_float') );
			
			static::$data							= get_user_meta(get_current_user_id(), "EB_Float_data");
			if(is_array(static::$data))				
			{
				static::$data						= array();
				update_user_meta(get_current_user_id(), "EB_Float_data", static::$data);
			}
			
			add_action('wp_footer',					array($this, 'add_float'), 20);
		}
		static function add_window($data)
		{
			if(!is_array($data))	$data = array();
			$data['width']			= $data['width'] 	? $data['width'] 	: 300;
			$data['height']			= $data['height']	? $data['height']	: 300;
			$data['top']			= $data['top']		? $data['top']		: 50;
			$data['left']			= $data['left']		? $data['left']		: 30;
			$data['title']			= $data['title']	? $data['title']	: __("new window", ERMAK_BOOKER);
			if(!is_array(static::$data))				static::$data = array();
			$data['tid']			= count(static::$data) + 1;
			array_push(static::$data, $data);
			update_user_meta(get_current_user_id(), "EB_Float_data", static::$data);
			$size		= 20;
			$dd			= array();
			if(count($data['title_switcher']))
			{
				foreach($data['title_switcher'] as $titlee)
				{
					$dd[]	= array('title'=>$titlee['title'], 'exec'=>$titlee['exec'], 'args'=>$titlee['args'], 'slide'=>'');
				}
			}
			return "
			<div id='eb_float_".$data['tid'] ."' slug='" . $data['id'] . "' class='eb_float_wigetc' STYLE='width:". $data['width']."px; height:".$data['height']."px; top:".$data['top']."px; left:".$data['left']."px;'>
					<div class='eb_float_title'>".
						$data['title'].
						"<div class='eb_float_title_cont'>" . Assistants::get_switcher($dd, "switcher_" . $data['id']) . "</div>
					</div>
					<div class='eb_float_cont' STYLE='width:".( $data['width'] - 5)."px; height:".( $data['height'] - 50 )."px;'>".
						$data['content'].
					"</div>
					<div class='eb_float_close dashicons'></div>
					<div class='eb_float_shpile dashicons' title='".__("Fix this window", ERMAK_BOOKER)."'></div>
					<div class='eb_float_sizer'>
						<svg width=$size height=$size xmlns='http://www.w3.org/2000/svg'>
							<g>
								<path fill='#000000' d='M0 $size L$size $size $size 0 0 $size'/>
							</g>
						</svg>
					</div>
			</div>";
		}
		function add_float()
		{
			foreach(static::$data as $data)
			{	
				
				?>			
				<div id='eb_float_<?php echo $data['id'] ?>' class='eb_float_wigetc' STYLE='width:<?php echo $data['width'] ;?>px; height:<?php echo $data['height'] ;?>px; top:<?php echo $data['top'] ;?>px; left:<?php echo $data['left'] ;?>px;'>
					<div class='eb_float_title'><?php echo $data['title'] ;?></div>
					<div class='eb_float_close dashicons'></div>
				</div>
			<?php } ?>
				<script>
					var eb_drag = function(){};
					var eb_isdrag = false;
					var eb_curdrag, eb_res, eb_res_obj;
					var eb_tmouse= {top:0, left:0};
					(function( $ ) 
					{  
						$("body").prepend($(".eb_float_wigetc"));
						eb_drag  = function()
						{
							$(".eb_float_title").live({
								'touchstart mousedown':function(e)
								{
									eb_curdrag	= this;
									eb_isdrag 	= true;
									var offset 	= $(this).offset();
									eb_tmouse	= {top:(e.pageY - offset.top), left:(e.pageX - offset.left)};
									set_message("touch start");
								},
								"touchend mouseup":function(evt)
								{
									eb_curdrag = undefined;
									eb_isdrag = false;
									set_message("touch ends");
								}
							});
							$(eb_curdrag).live({"touchmove":function(evt)
							{
								var pos				= { left:(evt.pageX - eb_tmouse.left), top:(evt.pageY - eb_tmouse.top)};
								$(this).parent().offset(pos);
							}});
							$("html").live({"touchmove mousemove":function(evt)
							{
								if(eb_isdrag)
								{
									var pos				= { left:(evt.pageX - eb_tmouse.left), top:(evt.pageY - eb_tmouse.top)};
									$(eb_curdrag).parent().offset(pos);
									set_message("touch move");
								}
								if(eb_res)
								{
									var obj				= $(eb_res_obj).parent();
									var cont			= $(eb_res_obj).parent().children(".eb_float_cont");
									var pos				= { left:(evt.pageX - 10), top:(evt.pageY - 10)};
									$(eb_res_obj).offset(pos);
									var ofser			= $(obj).offset();
									$(obj).css({width:(pos.left - ofser.left  + 20) + "px", height:(pos.top - ofser.top + 20) + "px"});
									$(cont).css({width:(pos.left - ofser.left + 15) + "px", height:(pos.top - ofser.top - 30) + "px"});
								}
							}});
							$(".eb_float_close").live({click:function(evt)
							{
								$(this).parent().detach();
							}});
							$(".eb_float_sizer").live({
								'touchstart mousedown':function(evt)
								{
									eb_res		= true;
									eb_res_obj 	= this;
									
								},
								"touchend mouseup" :function(evt)
								{
									eb_res 		= false;
									var offset	= $(eb_res_obj).offset();
									var left	= offset.left + $(eb_res_obj).width() -20;
									var top		= offset.top + $(eb_res_obj).height() -25;
									$(this).offset( {top: top, left: left} );
									eb_res_obj 	= undefined;
								}
							});
						}
						
						eb_drag();
					})(jQuery);
				</script>
			<?php
		}
		/**
		 * Display widget.
		 *
		 * @param array $args
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function widget( $args, $instance ) 
		{
		/*
			extract( $args );
			$instance['title'] ? NULL : $instance['title'] = '';
			$title = apply_filters('widget_title', $instance['title']);
			$output = $before_widget."\n";
			if($title)
				$output .= $before_title.$title.$after_title;
			
			$output .= $after_widget."\n";
			echo $output;
			*/
		}
		
		/**
		 * Configuration form.
		 *
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function form( $instance ) 
		{
			$defaults = array(
				'title' 			=> __('All Medias', "ermak_miph"),		
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>				
				<div class="alx-options-posts">
					<p>
						<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label>
						<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
					</p>		 
				</div>
			<?php
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $instance
		 * @return array
		 * @since 1.5.6
		 */
		function update( $new_instance, $old_instance )
		{
			$instance = $old_instance;
			$instance['title'] 			= strip_tags($new_instance['title']);		
			return $instance;
		}
	}
	
/*  Register widget
/* ------------------------------------ */
	function ermak_float_register_widget() { 
		register_widget( 'Ermak_Float_Widget' );
	}
	add_action( 'widgets_init', 'ermak_float_register_widget' );
	

