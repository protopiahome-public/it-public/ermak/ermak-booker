<?php
	class Ermak_Booker_Plugin
	{
		static $instance;
		public $options;
		static function get_instance()
		{
			if(!static::$instance)
				static::$instance	= new Ermak_Booker_Plugin;
			return static::$instance;
		}
		function __construct()
		{
			$this->options			= get_option(ERMAK_BOOKER);
			add_filter( 'smc_top_menu_position',		array($this, 'top_menu'));
			add_filter( 'smc_top_menu',					array($this, 'smp_top_menu'), 5);
			add_action( 'admin_enqueue_scripts', 		array($this, 'add_frons_js_script') );
			add_action( 'wp_enqueue_scripts', 			array($this, 'add_frons_js_script') );
			add_action( 'smc_myajax_submit', 			array($this, 'smc_myajax_submit'),15);
			add_action( 'admin_menu',					array($this, 'metagame_menu') , 31);
			add_action( 'wp_footer',					array($this, 'wp_footer'), 1);
			add_filter( 'template_include', 			array($this, 'my_template'), 9, 1);
			add_filter( "ermak_body_before", 			array($this, "ermak_body_before"),7);// null template content
			add_action( 'smc_add_object_type',			array(__CLASS__, 'smc_add_object_type') , 16);
			//
			add_filter('smp_change_table_goods_batch_par', array(__CLASS__, 'smp_change_table_goods_batch_par'), 10, 3);
		}
		
		
		static function smc_add_object_type($array)
		{
			$epg								= array();
			$epg['t']							= array('type' => 'post');
			$epg['consume_scheme_id']			= array('type' => 'id', 'object' => SMCO_CONSUME_SCHEME_TYPE);
			$epg['picto_id']					= array('type' => 'media', 	'download'=>true);
			$array[ERMAK_POPULATION_GROUP_TYPE]	= $epg;
			
			return $array;
		}
		
		function metagame_menu() 
		{
			add_submenu_page( 
								  'metagame'  //or 'options.php' 
								, __('Booker', ERMAK_BOOKER)							
								, __('Booker', ERMAK_BOOKER)
								, 'manage_options'
								, 'ermak_booker_sub_menu'
								, array($this,'get_submenu_page')
							);
		}
		function get_submenu_page()
		{
			global $Soling_Metagame_Constructor, $SolingMetagameProduction;
			$circle		= new Cyrcle_menu;
			$buttons	= apply_filters("eb_circle_menu_elements", array());
			$eb_button_hidden = get_option("eb_button_hidden");
			if(!is_array($eb_button_hidden))
				$eb_button_hidden= array();
			if($_POST['update_users'])
			{
				$this->options['home'] 		= $_POST['home'];
				$this->options['menu_type'] = $_POST['menu_type'];
				$this->options['curloc'] 	= $_POST['locdisplay_id'];
				update_option(ERMAK_BOOKER, $this->options);
			}
			if($_POST['save'])
			{
				$this->options['mobile_slides']		= array();
				if($_POST['per_mobile'])	$this->options['mobile_slides'][]	= $_POST['per_mobile'];
				if($_POST['gt_mobile'])		$this->options['mobile_slides'][]	= $_POST['gt_mobile'];
				if($_POST['store_mobile'])	$this->options['mobile_slides'][]	= $_POST['store_mobile'];
				if($_POST['pr_mobile'])		$this->options['mobile_slides'][]	= $_POST['pr_mobile'];
				if($_POST['pr_bank'])		$this->options['mobile_slides'][]	= $_POST['pr_bank'];
				do_action("ermak_booker_mobile_admin_save");
				//
				if($_POST['gb_setting'])
				{
					if(!is_array($this->options['gb_setting'])) 
						$this->options['gb_setting'] = array();
					foreach($_POST['gb_setting'] as $key=>$val)
					{
						$this->options['gb_setting'][$key] = (int)$val;
					}
				}
				if($_POST['store_setting'])
				{
					if(!is_array($this->options['store_setting'])) 
						$this->options['store_setting'] = array();
					foreach($_POST['store_setting'] as $key=>$val)
					{
						$this->options['store_setting'][$key] = (int)$val;
					}
				}
				update_option(ERMAK_BOOKER, $this->options);
			}
			if($_POST['cyhiddensave'])
			{
				$eb_button_hidden			= array();
				foreach($buttons as $button)
				{
					$nm					= $button['slug'];
					$eb_button_hidden[$nm]	= $_POST["cyhidden$nm"] != "on";
				}
				//echo Assistants::echo_me($eb_button_hidden);
				update_option("eb_button_hidden", $eb_button_hidden);
			}
			//echo  Assistants::echo_me($this->options['mobile_slides']);
			
			$html0			= "<h1>" . __("Settings") . "</h1>";
			$html1			= "<board_title>" . __("Desktop Special Page Parameters", ERMAK_BOOKER) . "</board_title>
				<form name='settings'  method='post'  enctype='multipart/form-data' id='settings'>
			<div class=smc_form>
				<div id='eb_main_desktop'>
					<board_title>".
						__("Contents", ERMAK_BOOKER).
					"</board_title>
					<div>
						<input type='radio' class='css-checkbox' id='home_0' name='home' value='0' " . checked((int)$this->options['home'], 0, 0) . "/>
						<label for='home_0' class='css-label'>".__("Standart Wordpress page", ERMAK_BOOKER). "</label>
					</div>
					<div>
						<input type='radio' class='css-checkbox' id='home_1' name='home' value='1' " . checked((int)$this->options['home'], 1, 0) . "/>
						<label for='home_1' class='css-label'>".__("Only mobile version (Personal functions)", ERMAK_BOOKER). "</label>
					</div>
					<div>
						<input type='radio' class='css-checkbox' id='home_2' name='home' value='2' " . checked((int)$this->options['home'], 2, 0) . "/>
						<label for='home_2' class='css-label'>".__("Only fullscreen Map panel", ERMAK_BOOKER). "</label><br>
					</div>
					<div>
						<input type='radio' class='css-checkbox' id='home_3' name='home' value='3' " . checked((int)$this->options['home'], 3, 0) . "/>
						<label for='home_3' class='css-label'>".__("Panel of the Location", ERMAK_BOOKER). "</label>
					</div>
					<p></p>
				</div>
				<hr/>
				<div id='eb_menu_type' class='ellemm'>
					<board_title>".
						__("Type of menu", ERMAK_BOOKER).
					"</board_title>".
					
						"<input type='radio' class='css-checkbox' id='type_0' name='menu_type' value='".CIRCLE_MENU_TYPE."' " . checked($this->options['menu_type'], CIRCLE_MENU_TYPE, 0) . "/>
						<label for='type_0' class='css-label'>".__("Weel (Game style)", ERMAK_BOOKER). "</label><br>
						<input type='radio' class='css-checkbox' id='type_1' name='menu_type' value='".TOP_MENU_TYPE."' " . checked($this->options['menu_type'], TOP_MENU_TYPE, 0) . "/>
						<label for='type_1' class='css-label'>".__("Top Menu (Application style)", ERMAK_BOOKER). "</label><br>
						<p></p>".
					"
				</div>
				<div id='eb_chsloc' class='ellemm'>
					<board_title>".
						__("choose Location", ERMAK_BOOKER).
					"</board_title>".
						$Soling_Metagame_Constructor->get_location_selecter(array('name'=>'locdisplay_id', 'selected'=>$this->options['curloc'])).
					"<p></p>
				</div>
				<input type='submit' class=button name=update_users value='".__("Update")."'/>
			</div>";
			
			/*
			//	Mobile settings
			*/
			$checked0	= in_array(EB_PERSONAL_MOBILE_PAGE,		$this->options['mobile_slides']);
			$checked1	= in_array(EB_PRODUCTS_MOBILE_PAGE,		$this->options['mobile_slides']);
			$checked2	= in_array(EB_GOODS_BATCHS_MOBILE_PAGE,	$this->options['mobile_slides']);
			$checked3	= in_array(EB_STORE_MOBILE_PAGE, 		$this->options['mobile_slides']);
			$checked4	= in_array(EB_BANK_MOBILE_PAGE, 		$this->options['mobile_slides']);
			
			if(!is_array($this->options['gb_setting']))	
				$this->options['gb_setting'] = array('is_picto'=>1, 'is_owner'=>1, 'is_disl'=>0, 'is_sale'=>0);
			$is_picto	= $this->options['gb_setting']['is_picto']		? "" : "eb_disbl";
			$is_owner	= $this->options['gb_setting']['is_owner'] 		? "" : "eb_disbl";
			$is_disl	= $this->options['gb_setting']['is_disl'] && $SolingMetagameProduction->is_dislocation()	? "" : "eb_disbl";
			$is_sale	= $this->options['gb_setting']['is_sale'] 		? "" : "eb_disbl";
			$picto		= $this->options['gb_setting']['is_picto'];
			$owner		= $this->options['gb_setting']['is_owner'];
			$disl		= $this->options['gb_setting']['is_disl'];
			$sale		= $this->options['gb_setting']['is_sale'];
			
			if(!is_array($this->options['store_setting']))	
				$this->options['store_setting'] = array('is_picto'=>1, 'is_disl'=>1, 'is_owner'=>0, 'is_sale'=>1);
			$sis_picto	= $this->options['store_setting']['is_picto']	? "" : "eb_disbl";
			$sis_owner	= $this->options['store_setting']['is_owner'] 	? "" : "eb_disbl";
			$sis_disl	= $this->options['store_setting']['is_disl'] && $SolingMetagameProduction->is_dislocation()	? "" : "eb_disbl";
			$sis_sale	= $this->options['store_setting']['is_sale'] 	? "" : "eb_disbl";
			$spicto		= $this->options['store_setting']['is_picto'];
			$sowner		= $this->options['store_setting']['is_owner'];
			$sdisl		= $this->options['store_setting']['is_disl'];
			$ssale		= $this->options['store_setting']['is_sale'];
			$html2		= "<board_title>" . __("Mobile Special Page Parameters", ERMAK_BOOKER) . "</board_title>
			<div class=smc_form>
				<board_title>" . __("Enabled pages", ERMAK_BOOKER) . "</board_title>
				<form name='settings'  method='post'  enctype='multipart/form-data' id='settings'>
					<p>
						<input type='checkbox' class='checkbox' name='per_mobile' id='per_mobile' value='".EB_PERSONAL_MOBILE_PAGE."' ".checked($checked0, 1, 0) . "/>
						<label for='per_mobile'>".__("Player", "smc"). "</label>						
						<div id='_per_mobile' class='h4 " . ( !$checked0 ? "lp-hide" : "" ) . "'>
							<board_title>".__("Player", "smc"). "</board_title>
							<p class='smc-description'>".__("This inset hasn't settings.", ERMAK_BOOKER)."</p>
						</div>
					</p>
					<p>
						<input type='checkbox' class='checkbox' name='pr_bank' id='pr_bank' value='".EB_BANK_MOBILE_PAGE."' ".checked($checked4, 1, 0) . "/>
						<label for='pr_bank'>".__("Bank", "smp"). "</label>
						<div id='_pr_bank' class='h4 " . ( !$checked4 ? "lp-hide" : "" ) . "'>
							<board_title>".__("Bank", "smp"). "</board_title>
							<p class='smc-description'>".__("This inset hasn't settings.", ERMAK_BOOKER)."</p>
						</div>
					</p>
					<p>
						<input type='checkbox' class='checkbox' name='gt_mobile' id='gt_mobile' value='".EB_GOODS_BATCHS_MOBILE_PAGE."' ".checked($checked2, 1, 0) . "/>
						<label for='gt_mobile'>".__("Goods Batchs", ERMAK_BOOKER). "</label>
						<div id='_gt_mobile' class='h4 " . ( !$checked2 ? "lp-hide" : "" ) . "'>
							<board_title>".__("Goods Batchs", ERMAK_BOOKER). "</board_title>
							<p class='smc-description'>".__("Set columns for show in interface. Theese parameters may change by Player.", ERMAK_BOOKER)."</p>
							<table class='eb_manage_table' cellspacing='1' sellpadding='4'>
								<tr>
									<td>
										<div class='eb_managegbpicto $is_picto' dsbll='1' id='is_picto' prefix='gb_setting'><span>".__("Goods Type icon", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='gb_setting[is_picto]' value='$picto'/>
									</td>
									<td>
										<div class='eb_managegbpicto $is_owner' id='is_owner' prefix='gb_setting'><span>".__("Batch's owner", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='gb_setting[is_owner]' value='$owner'/>
									</td>
									<td>
										<div class='eb_managegbpicto $is_disl' id='is_disl' prefix='gb_setting'><span>".__("Batch's location", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='gb_setting[is_disl]' value='$disl'/>
									</td>
									<td>
										<div class='eb_managegbpicto $is_sale' id='is_sale' prefix='gb_setting'><span>".__("Sale info", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='gb_setting[is_sale]' value='$sale'/>
									</td>".
									apply_filters("eb_goods_batch_setting_rows", "").
								"</tr>
							</table>
						</div>
					</p>
					<p>
						<input type='checkbox' class='checkbox' name='store_mobile' id='store_mobile' value='".EB_STORE_MOBILE_PAGE."' ".checked($checked3, 1, 0) . "/>
						<label for='store_mobile'>".__("Stores", "smp"). "</label>
						<div id='_store_mobile' class='h4 " . ( !$checked3 ? "lp-hide" : "" ) . "'>
							<board_title>".__("Stores", "smp"). "</board_title>
							<p class='smc-description'>".__("Set columns for show in interface. Theese parameters may change by Player.", ERMAK_BOOKER)."</p>
							<table class='eb_manage_table' cellspacing='1' sellpadding='4'>
								<tr>
									<td>
										<div class='eb_managegbpicto $sis_picto' dsbll='1' id='is_picto' prefix='store_setting'><span>".__("Goods Type icon", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='store_setting[is_picto]' value='$spicto'/>
									</td>
									<td>
										<div class='eb_managegbpicto $sis_owner' id='is_owner' prefix='store_setting'><span>".__("Batch's owner", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='store_setting[is_owner]' value='$sowner'/>
									</td>
									<td>
										<div class='eb_managegbpicto $sis_disl' id='is_disl' prefix='store_setting'><span>".__("Batch's location", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='store_setting[is_disl]' value='$sdisl'/>
									</td>
									<td>
										<div class='eb_managegbpicto $sis_sale' id='is_sale' prefix='store_setting'><span>".__("Sale info", ERMAK_BOOKER)."</span></div>
										<input type='hidden' name='store_setting[is_sale]' value='$ssale'/>
									</td>".
									apply_filters("eb_store_setting_rows", "").
								"</tr>
							</table>
						</div>
					</p>
					<p>
						<input type='checkbox' class='checkbox' name='pr_mobile' id='pr_mobile' value='".EB_PRODUCTS_MOBILE_PAGE."' ".checked($checked1, 1, 0) . "/>
						<label for='pr_mobile'>".__("Productions", ERMAK_BOOKER). "</label>
						<div id='_pr_mobile' class='h4 " . ( !$checked1 ? "lp-hide" : "" ) . "'>
							<board_title>".__("Productions", ERMAK_BOOKER). "</board_title>
							<p class='smc-description'>".__("This inset hasn't settings.", ERMAK_BOOKER)."</p>
						</div>
					</p>".
					apply_filters("ermak_booker_mobile_admin", "").
					"<input type='submit' name='save' value='".__("Save")."'/>
				</form>
			</div>";
			$html3		= "<board_title>" . __("Wheel's parameters", ERMAK_BOOKER) . "</board_title>
			<div class=smc_form>
			<p>
				<board_title>" . __("Visible of buttons", ERMAK_BOOKER) . "</board_title>";
			$i =0;
			foreach($buttons as $button)
			{
				$nm		= $button['slug'];
				$ttl	= $button['hint'];
				$html3	.= "
				<div>
					<input type='checkbox' name='cyhidden$nm' id='cyhidden$nm' class='css-checkbox' ". checked(1, !$eb_button_hidden[$nm], false) . "/>
					<label class='css-label' for='cyhidden$nm'>$ttl</label>
				</div>
				";
				$i++;
			}
			$html3		.= "</p>
			<input type='submit' name='cyhiddensave' value='".__("Save")."'/>
			</div>
			</form>
			";
			$arr		= array(
				array("title" => __("Desktop Special Page Parameters", ERMAK_BOOKER), 	"slide" => $html1),
				array("title" => __("Wheel's parameters", ERMAK_BOOKER), 				"slide" => $html3),
				array("title" => __("Mobile Special Page Parameters", ERMAK_BOOKER), 	"slide" => $html2)
			);
			$html		= $html0 . Assistants::get_switcher($arr, "booker_settings", array("class"=>"smc_switch_button lp-hide swbtn"))."
			<script>
				var change_tp		= function(){},
					show_weel_pars	= function(){},
					hide_weel_pars	= function(){};
				
				jQuery(function($)
				{
					
					show_weel_pars = function(evt)
					{
						$('#btn_booker_settings1').fadeIn();
					}
					hide_weel_pars = function(evt)
					{
						$('#btn_booker_settings1').hide();
					}
					change_tp = function()
					{
						$('.swbtn').each(function(n, elem)
						{
							if($(elem).attr('id') != 'btn_booker_settings1')
								$(elem).fadeIn();
						});
						$('.ellemm').hide();
						var al		= $('input[name=home]:checked').val();
						switch(al)
						{
							case '0':
								$('#eb_menu_type').fadeIn('slow');
								break;
							case '1':
								
								break;
							case '2':
								$('#eb_menu_type').fadeIn('slow');
								break;
							case '3':
								$('#eb_chsloc').fadeIn('slow');								
								break;
						}
					}
					$('input[name=home]:checked').live({change:function(evt)
					{
						change_tp()
					}});
					change_tp();
					
					$('.checkbox').change(function(evt)
					{
						var par	= '#_' + $(this).attr('id');
						if($(this).is(':checked'))
						{
							$(par).fadeIn('slow');
						}
						else
						{
							$(par).hide();
						}
					});
					$('.eb_managegbpicto').click(function(evt)
					{					
						if($(this).attr('dsbll')) return;
						var prefix		= $(this).attr('prefix');
						$(this).toggleClass('eb_disbl');						
						$('[name=\"' + prefix + '['+ $(this).attr('id') + ']\"]').val(!$(this).hasClass('eb_disbl') ? '1' : '0');
					});
					$($('[name=menu_type]')).change(function(evt)
					{
						if($('[name=menu_type]:checked').val() == '".CIRCLE_MENU_TYPE."' )
							show_weel_pars();
						else
							hide_weel_pars();
					});
					if($('[name=menu_type]:checked').val() == '".CIRCLE_MENU_TYPE."' && $('#eb_menu_type').is(':visible') == true)
					{
						show_weel_pars();
					}
				});
			</script>
			";
			echo $html;
		}
		static function activate()
		{
			$instance							= Ermak_Booker_Plugin::get_instance();
			$instance->options['menu_type'] 	= CIRCLE_MENU_TYPE;
			$instance->options['mobile_slides'] = array(
				EB_PERSONAL_MOBILE_PAGE, 
				EB_PRODUCTS_MOBILE_PAGE, 
				EB_GOODS_BATCHS_MOBILE_PAGE,
				EB_STORE_MOBILE_PAGE,
			);
			update_option(ERMAK_BOOKER, $instance->options);
		}
		static function deactivate()
		{
			delete_option(ERMAK_BOOKER);
		}
		function smp_top_menu($arr)
		{
			//factory
			$agts		= array();
			$agts[]		= array('title'=> "<span>".__("all Factories", "smp"). "</span>" , 'id'=>"all_factories");
			$agts[]		= array('title'=> "<span>".__("My Factories", "smp"). "</span>" , 'id'=>"my_factories");
			$arr[]		= array("slug"=>"factory", 'name'=>__("all Factories", "smp") . " <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span>", 'id'=>'fc', 'exec_send'=>'show_factories', 'exec'=>'show_factories');
			//goods batchs
			//$all_gt		= Goods_Type::get_global();
			$agts		= array();
			$agts[]		= array('title'=> "<span>".__("all Goods", "smp"). "</span>" , 'id'=>"all_goods_types");
			$agts[]		= array('title'=> "<span>".__("My Goods Batches", "smp"). "</span>" , 'id'=>"my_goods_types");
			$agts[]		= array('title'=> "<span>".__("Stores", "smp"). "</span>" , 'id'=>"store_goods_types");
			//foreach($all_gt as $gt)
			//{
			//	//$agts[]	= array('title'=> '<table><tr><td>'. get_the_post_thumbnail( $gt->ID, array(30,30))."</td><td>".$gt->post_title.'</td></tr></table>', 'id'=>$gt->ID);
			//}
			$arr[]		= array("slug"=>"goods_types", 'name'=>__("all Goods", "smp") . " <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span>", 'id'=>'gt', 'exec_send'=>'show_mine_gt', 'exec'=>'show_mine_gt');
			unset($agts);
			//hubs
			$aghs		= array();
			$aghs[]		= array('title'=> "<span>".__("all Hubs", "smp"). "</span>" , 'id'=>"all_hubs");
			$aghs[]		= array('title'=> "<span>".__("My Hubs", "smp"). "</span>" , 'id'=>"my_hubs");
			$title_switcher 	= array();
			$title_switcher[]	= array("title"=>__("All","smc"), 		"exec"=>"", "args"=> "");
			$title_switcher[]	= array("title"=>__("Only mine","smc"), "exec"=>"", "args"=> "");
			$arr[]		= array("slug"=>"hubs", 'name'=>__("all Hubs", "smp") . " <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span>", 'id'=>'fc', 'exec_send'=>'show_hubs', 'exec'=>'show_hubs');
			return $arr;
		}
		function my_template($template)
		{
			global $post, $is_mobile, $Soling_Metagame_Constructor;
			if(is_front_page())//if($post->ID == $Soling_Metagame_Constructor->options['personal_smartphone']) 
			{
				if($is_mobile || (int)$this->options['home'] != 0)
				{
					return SMC_REAL_PATH."template/empty.php";
				}
				else
				{
				
				}
			}
			return $template;
		}
		function ermak_body_before($text)
		{
			global $post, $is_mobile, $Soling_Metagame_Constructor;
			//switch($post->ID)
			//{
			//	case $Soling_Metagame_Constructor->options['personal_smartphone']:	
					if($is_mobile)
					{
						require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
						return $text .  personal_smartphone();
					}
					else if(is_front_page())
					{
						switch($this->options['home'] )
						{
							case 1:
								require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
								return $text .  personal_smartphone();
							case 2:
								return $Soling_Metagame_Constructor->add_location_interface(1200);
								//return $text .  "fullscreen map";
							case 3:
								//$loc			= SMC_Location::get_instance($this->options['curloc']);
								//return $text .  $loc->name;
								return draw_location_display($this->options['curloc']);
						}
						return "<h1>desktop</h1>";
					}
			//		break;
			//}
			return $text;
		}
		function wp_footer()
		{
			global $is_smc_login_widget;
			$is_smc_login_widget = true;
		}
		function top_menu_real()
		{	
			$menus		= apply_filters("smc_top_menu", 
			array(
				array("slug"=>"fullmenu", "hint"=>__("Full Screen", "smc"), 'name'=>"<i class='fa fa-arrows-alt'></i>", 'id'=>"top_fullscreen"),
				//array("slug"=>"add_win", "hint"=>__("Add Window", ERMAK_BOOKER), 'name'=>"<i class='fa fa-plus'></i>", 'id'=>"add_win"),
			));
			$html		= "<div id='top_menu'>";
			foreach($menus as $menu)
			{ 
				if($menu['hint'])
				{
					$hint=  "data-hint='".$menu['hint']."'";
					$class= " hint hint--bottom";
				}
				else
				{
					$hint=  "";
					$class= "";
				}
				if($menu['exec'])
				{
					$exec=  "exec='".$menu['exec']."'";
				}
				else
				{
					$exec=  "";
				}
				$dd			= array();
				if(count($menu['title_switcher']))
				{
					foreach($menu['title_switcher'] as $titlee)
					{
						$dd[]	= array('title'=>$titlee['title'], 'exec'=>$titlee['exec'], 'args'=>$titlee['args'], 'slide'=>'');
					}
				}
				if($menu['submenu'])
				{
					$submenu		= "<div class='top_submenu' exec_send='".$menu['exec_send']."'>";
					foreach($menu['submenu'] as $sb)
					{
						if($sb['com'])
							$com	= "<div class='tsmenucom' com='".$sb['com']."'>". $sb['com_picto']."</div>";
						$submenu	.= "<div class='tsmenuel' id='".$sb['id']."'>".$sb['title']."$com</div>" ;
					}
					$submenu		.= "</div>";
				}
				$html	.= "<div class='tmenuec'><div id='".$menu['id']."' class='tmenuelem $class' $hint $exec>". 
					$menu['name'].
				"</div>"."$submenu</div>";
			}
			$html		.= "</div>";
			return $html;
		}
		function top_menu()
		{
			switch($this->options['menu_type'])
			{
				case CIRCLE_MENU_TYPE:
					$circle	= new Cyrcle_menu;
					return $circle->draw();
				case TOP_MENU_TYPE:
				default:
					return $this->top_menu_real();
			}
		}
		
		//update table of goods batch after Player's changes
		static function smp_change_table_goods_batch_par($html, $gb_table_id, $gb_slide_id)
		{
			switch( $gb_table_id)
			{
				case "mine_gb":
					$html				= Goods_Batch::get_all_batchs_switcher(-1, true, "mine_gb", $gb_slide_id);
					break;
				case "all_gb":
					$html				= Goods_Batch::get_all_batchs_switcher(-1,false, "all_gb", $gb_slide_id);
					break;
				default:
					break;
			}
			return $html;
		}
		
		
		function add_frons_js_script()
		{			
			//css
			wp_register_style('ermak_booker_front', ERMAK_BOOKER_URLPATH . 'css/Ermak_booker_front.css', array());
			wp_enqueue_style( 'ermak_booker_front');
			
			//js
			wp_register_script('ermak_booker_front_js', plugins_url( '../js/Ermak_booker_front.js', __FILE__ ), array());
			wp_enqueue_script('ermak_booker_front_js');						
		}
		function smc_myajax_submit($params)		
		{	
			global $start, $Soling_Metagame_Constructor, $smc_height, $Factory;
			$html			= 'none!';
			switch($params[0])
			{
				case 'move_circle_menu':
					$top				= $params[1];
					$left				= $params[2];
					$user_id			= get_current_user_id();
					$data				= get_user_meta($user_id, ERMAK_BOOKER);
					$data[0]			= $data;
					if(!is_array($data))
					{
						$data			= array();
					}
					$data['top']		= $top;
					$data['left']		= $left;
					$is					= update_user_meta($user_id, ERMAK_BOOKER, $data);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $is,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'eb_add_window':
					$data				= $params[1];
					$user_id			= get_current_user_id();
					$win				= Ermak_Float_Widget::add_window($user_id, $data);
					$d					= array(	
													$params[0], 
													array(
															'new_win'	=> $win,
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_hubs':
					$goods_type_id		= $params[1];
					$html				= get_hubs_list();
					$title				= __("My Hubs", "smp");
					$title_switcher		= array();
					$title_switcher[]	= array("title"=>__("only mine", "smp"),	"exec"=>"show_hubs_cont", "args"=>"mine,hb");
					$title_switcher[]	= array("title"=>__("all", "smp"), 			"exec"=>"show_hubs_cont", "args"=>"all,hb");					
					$d					= array(	
													$params[0], 
													array(
															'text'		=> Ermak_Float_Widget::add_window(array('id'=>'hb', 'content'=>$html, "title"=> $title, "width" => 600, "height"=> $smc_height, "title_switcher"=>$title_switcher )),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_hubs_cont':
					$goods_type_id		= $params[1];
					$html			= $params[1] == 'mine' ? get_hubs_list() : get_hubs_list(false);					
					$d					= array(	
													$params[0], 
													array(
															'text'		=> $html,
															'cont'		=> $goods_type_id[1],
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_factories':
					$html				= my_production_page();
					$title				= __("all Factories", "smp");
					$title_switcher		= array();
					$title_switcher[]	= array("title"=>__("only mine", "smp"),	"exec"=>"show_factory_cont", "args"=>"mine,fc");
					$title_switcher[]	= array("title"=>__("all", "smp"), 			"exec"=>"show_factory_cont", "args"=>"all,fc");	
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'fc', 'content'=>$html, "title"=> $title, "width" => 600, "height"=> $smc_height, "title_switcher"=>$title_switcher  )),
															//'text' 		=> array($title, $html),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_factory_cont':
					$goods_type_id		= $params[1];
					if($goods_type_id 	== "mine")
					{
						$html			= my_production_page();
					}
					else
					{
						$html			= my_production_page(false);
					}
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $html,
															'cont'	=> $goods_type_id[1],
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_mine_gt':	
					$html				= Goods_Batch::get_all_batchs_switcher(-1, true, "mine_gb");
					$title				= __("all Goods", "smp");
					$title_switcher		= array();
					$title_switcher[]	= array("title"=>__("only mine", "smp"), 	"exec"=>"show_gt_cont", "args"=>"my_goods_types,gt");
					$title_switcher[]	= array("title"=>__("all", "smp"), 			"exec"=>"show_gt_cont", "args"=>"all_goods_types,gt");
					$title_switcher[]	= array("title"=>__("Stores", "smp"), 		"exec"=>"show_gt_cont", "args"=>"store_goods_types,gt");
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'gt', 'content'=>$html, "title"=> $title, "width" => 500, 'height'=>$smc_height, "title_switcher"=>$title_switcher )),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'show_gt_cont':
					$goods_type_id		= $params[1];
					if($goods_type_id[0] == "my_goods_types")
					{
						$html				= Goods_Batch::get_all_batchs_switcher(-1, true, "mine_gb");
						$title				= __("My Goods Batches", "smp");
					}
					else if($goods_type_id[0] == "all_goods_types")
					{
						$html				= Goods_Batch::get_all_batchs_switcher(-1,false, "all_gb");
						$title				= __("all Goods", "smp");
					}
					else if($goods_type_id[0] == "store_goods_types")
					{
						$html				= Goods_Batch::get_all_batchs_switcher( array( "store" => 1 ), false, "store");
						$title				= __("Stores", "smp");
					}
					else
					{
						$gt					= Goods_Type::get_instance($goods_type_id[0]);
						$html				= "<table class='goods_type_tbl'>";
						$args				= array("goods_type_id"=>$goods_type_id[0]);
						$all_goods_batchses	= Goods_Batch::get_my_batches($args);
						foreach($all_goods_batchses as $gb)
						{
							$b				= SMP_Goods_Batch::get_instance($gb);
							$html			.= $b->get_table_row();
						}					
						$html				.= "</table>";
						$title				= $gt->post_title;
					}					
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $html,
															'cont'	=> $goods_type_id[1],
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "eb_show_locations":
					$html				= get_location_page();
					$title				= __("Locations", "smc");
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'lct', 'content'=>$html, "title"=> $title, "width" => 800, "height"=> $smc_height )),
															//'text' 		=> array($title, $html),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "eb_show_bank":
					$html				= get_bank_page();
					$title				= __("Bank", "smp");
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'bk', 'content'=>$html, "title"=> $title, "width" => 600, "height"=> $smc_height  )),
															//'text' 		=> array($title, $html),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				// 
				case "eb_show_tools":
					$html				= smp_my_tools();
					$title				= __("My Tools", "smp");
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'tl', 'content'=>$html, "title"=> $title, "width" => 600, "height"=> $smc_height  )),
															//'text' 		=> array($title, $html),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				// MOBILE
				case "eb_refresh_mobile":
					require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
					$slide_id			= $params[1];
					$inset_id			= $params[2];
					list($slide, $data)	= refresh_slide($slide_id, false, $inset_id);
					$d					= array(	
													$params[0], 
													array(
															'text'		=> $slide,
															'slide_id'	=> $slide_id, 
															'time'		=> ( getmicrotime()  - $start ),
															'inset_id'	=> $inset_id,
															'data'		=> $data														
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				// MOBILE: put or remove from Store 
				case "eb_change_store";
					require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
					$id			= $params[1];
					$slide_id	= $params[3];
					$inset_id	= $params[4];
					$batch		= SMP_Goods_Batch::get_instance($id);	
					if($batch->user_is_owner())
					{
						$html	= "";		
						$store	= $batch->get_meta("store");	
						$batch->update_meta("store", !$store);
					}
					else
					{
						$a_alert= __("You are not owner of this batch!", "smp");
					}
					$d			= array(	
											$params[0], 
											array(
													'text'		=> refresh_slide($slide_id, false, $inset_id),
													'slide_id'	=> $slide_id, 
													'a_alert'	=> $a_alert,
													'inset_id'	=> $inset_id,					
													'args'		=> $bid
												 )
										);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				// MOBILE: pay goods batch
				case "eb_pay_gb":
					require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
					$gb_id				= $params[1];
					$acc_id				= $params[2];
					$summ				= $params[3];
					$slide_id			= $params[4];
					$inset_id			= $params[5];
					if(is_user_logged_in() )
					{
						$user_id		= get_current_user_id();
						$gb				= SMP_Goods_Batch::get_instance( $gb_id );
						$locs			= $Soling_Metagame_Constructor->all_user_locations($user_id);
						$loc			= $locs[0];
						if($loc)
						{
							$gb->update_meta("owner_id", 	$loc);
							$gb->update_meta("store",		false);
							//$alert			= Assistants::echo_me($loc);
						}
						else
						{
							$alert		= __("You can not make economic transaction. The reason - lack of ownership. Call Master.", "smc"); 
						}
					}
					else
						$alert			= __("Log in please.", "smc");
					$d					= array(	
													$params[0], 
													array(
															'text'		=> refresh_slide($slide_id, false, $inset_id),
															'slide_id'	=> $slide_id, 
															'inset_id'	=> $inset_id,
															"a_alert"	=> $alert,
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				// MOBILE: change productivity of factory
				case "eb_chng_fct_prdct":	
					require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
					$fid				= $params[1];
					$powerfull			= $params[2];
					$slide_id			= $params[3];
					if( Factories::is_user_owner( get_post_meta($fid, "owner_id", true), get_current_user_id() ) )
					{
						$factory		= Factory::get_factory($fid);
						$new_p			= $factory->update_productivity($powerfull); 
					}
					$d					= array(	
													$params[0], 
													array(
															'text'		=> refresh_slide(EB_PRODUCTS_MOBILE_PAGE, false),
															'fid'		=> $fid,
															'slide_id'	=> $slide_id,
															'product'	=> $factory->get_productivity(),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				// MOBILE: change factory goods type 
				case "eb_chng_fctgt":	
					require_once(ERMAK_BOOKER_REAL_PATH."shortcodes/personal_smartphone.php");
					$fid				= $params[1];
					$goods_type_id		= $params[2];
					$slide_id			= $params[3];
					if( Factories::is_user_owner( get_post_meta($fid, "owner_id", true), get_current_user_id() ) )
					{
						$Factory->update_goods_type($fid, $goods_type_id );
						$factory		= Factory::get_factory($fid, true);
					}
					$d					= array(	
													$params[0], 
													array(
															'product'	=> $factory->get_productivity(),
															'text'		=> refresh_slide(EB_PRODUCTS_MOBILE_PAGE, false),
															'fid'		=> $fid,
															'slide_id'	=> $slide_id,
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
			}
		}
	}
?>