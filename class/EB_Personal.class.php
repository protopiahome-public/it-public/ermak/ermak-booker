<?php
	class EB_Personal
	{
		function __construct()
		{
			
		}
		static function gb_pay_form($gb_id)
		{
			global $SMP_Currency, $SolingMetagameProduction;
			$goods_batch	= SMP_Goods_Batch::get_instance($gb_id);
			$summ			= $goods_batch->get_meta("price");
			$ct_id			= $goods_batch->get_meta("currency_type_id");
			if($ct_id=="" || (int)$summ==0 || !$SolingMetagameProduction->is_finance())
			{
				$html		= "<div>
					<div class='button take_gb_button' gbid='$gb_id'>".__("Take", "smp"). "</div>
				</div>";
			}
			else
			{
				$price		= $SMP_Currency->get_price($ct_id, $summ);
				$html		= "
				<div>					
					<div class='button pay_gb_button pay_gbbt' gbid='$gb_id'>
					<span class='smp_gb_price' >".
						$price ."
					</span><BR>".__("Pay", "smp"). "</div>
				</div>";
			}
			return $html;
		}
		
		/*
			$id			- batch ID
			$prefix		- prefix for id input element for jQuery manipulatings
			$option_data- $this->options setting for filtered rows ($this->options['gb_setting'] || $this->options['store_setting'])
		*/
		static function get_smp_goods_batch_form($id, $prefix="", $option_data=-1)
		{
			global $SMP_Currency, $Soling_Metagame_Constructor, $SolingMetagameProduction, $SMP_Assistants;
			$gb				= SMP_Goods_Batch::get_instance($id);
			if(!is_array($add_row))		$add_row 		= array();
			if(!is_array($option_data))	$option_data 	= Ermak_Booker_Plugin::get_instance()->options['gb_setting'];
			$id				= $gb->id;
			$is_block		= $gb->get_meta( "is_blocked");
			$dis_id			= $gb->get_meta( 'dislocation_id');
			$owner_id		= $gb->get_meta( 'owner_id');
			$quality		= $gb->get_meta( 'quality');
			$factory_id		= $gb->get_meta("factory_id");
			$store			= $gb->get_meta("store");
			$location		= SMC_Location::get_instance($dis_id);
			$owner			= SMC_Location::get_instance($owner_id);
			$owner_type		= $Soling_Metagame_Constructor->get_location_type($owner_id);
			$owner_text		= '<span class="lp-batch-location_type">'.
									$owner_type->post_title.
								'</span>
								<span>'.
									$owner->name.
								'</span>';
			$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			$count			= $gb->get_meta('count');
			$factory		= Factory::get_factory($factory_id);
			$fac_name		= $factory_id!="" ? $factory->body->post_title : __("not indicated", "smp");
			
			$goods_type_id	= $gb->get_meta('goods_type_id');			
			$best_before	= $gb->get_meta("best_before");
			$is_trash		= $best_before<0 && $SolingMetagameProduction->is_best_before();
			$color			= get_post_meta($goods_type_id, "color", true);
			$bg				= !$is_trash ? "#FFF"  : "url(".SMP_URLPATH."img/rja.jpg)";
			$gt				= Goods_Type::get_instance($goods_type_id);
			$best_before_text = $best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") :  $best_before . " " . __("circles", "smp");
			$your			= $is_owner ? '<div style="height:20px;"></div><div class=smc-your-label style="bottom:0; right:-10px;">'.__("It's your", "smc").'</div>' : "";
			$bg				= $is_owner	? " style='background:rgba(0,0,0,0.25)' " : "";
			if(is_user_logged_in())
			{
				$checked	= $is_owner	? "<input type='checkbox' class='checkbox' storgbid='$id' id='checkbox".$prefix.$id."' " . checked($store, 1, 0) . "/><label for='checkbox".$prefix.$id."'></label>" : ( $store ? static::gb_pay_form($id)  : __("No pay") );
			}
			else
			{
				$summ			= $gb->get_meta("price");
				$ct_id			= $gb->get_meta("currency_type_id");
				if($ct_id=="" || (int)$summ==0 || !$SolingMetagameProduction->is_finance())
				{
					$html		.= "
					<div>
						--
					</div>";
				}
				else
				{
					$price		= $SMP_Currency->get_price($ct_id, $summ);
					$html		.= "
					<div>	
						<span class='smp_gb_price' >".
							$price ."
						</span>
					</div>";
				}
				$checked	= $html;
			}
			$class			= $is_owner ? "execgbtch" : "execgbtch";
			$disloc			= SMC_Location::get_instance($dis_id); 
			$disloc_type	= $Soling_Metagame_Constructor->get_location_type($dis_id);
			$disl_text		= '<span class="lp-batch-location_type">'.
									$disloc_type->post_title.
								'</span>
								<span>'.
									$disloc->name.
								'</span>';
			$add_titles		= "";
			$add_contents	= "";
			$picto					= SMP_Goods_Batch::get_picto($id, false, array("size"=>70, "left_text"=>"litle", "is_menu"=>false));
			$html					= "			
				<tr $bg>";
			$html					.= $option_data['is_picto'] ? "	
					<td>
						<div class='button smc_padding execgbtch' style='padding:3px!important;'  gbid='$id'>".
							$picto.
						"</div>
					</td>" : "";
			$html					.= $option_data['is_owner'] ? "	
					<td>".
						$owner_text.
					"</td>" : "";
			$html					.= $option_data['is_disl'] ? 
					"<td>".
						$disl_text.
					"</td>" : "";
			$html					.= $option_data['is_sale'] ? "
					<td style='width:50px;'>
						$checked
					</td>": "";
			$html					.=  "
				</tr>";
			
			//$html					.= "</table>";
			return $html;
			
		}
		/*
			$meta		- meta_query paramters for filtered wb_query
			$only_mine	- only current user owned
			$name		- id of switcher
			$slide_id	- ID of global page in Mobile JSSOR Slider
		*/
		static function get_my_storage_form($meta=-1, $only_mine=true, $name="my_gbss", $slide_id=0)
		{
			global $user_iface_color, $current_goods_type;
			$all_goods_batchses		= Goods_Batch::get_all_batches($meta, $only_mine, $name);
			$all_goods_types		= Goods_Type::get_global();
			$sorted_batches			= array();
			foreach($all_goods_types as $goods_type) 
			{
				$sorted_batches[(string)$goods_type->ID]= array();
			}
			
			$arr		= array();
			foreach($all_goods_types as $current_goods_type)
			{
				$sorted_batches[(string)$current_goods_type->ID]	= array_filter($all_goods_batchses, "by_type");
				//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
				//var_dump($sorted_batches[(string)$current_goods_type->ID]);
				
			}
			$option_data	= Ermak_Booker_Plugin::get_instance()->options['gb_setting'];			
			$i=0;
			foreach($all_goods_types as $goods_type)
			{
				$title		=  Goods_Type::get_picto($goods_type->ID, 42, -1, count($sorted_batches[(string)$goods_type->ID]) );
				$slide		= "<div>";
				$cb			= $sorted_batches[(string)$goods_type->ID];
				$i = 0;
				$slide		.= '<h3><span style=\'font-weight:700; color:#AAA!important\'>'. __("Goods type", "smp").'</span> ' . $goods_type->post_title.'</h3>' ;
				$slide		.= "
				<div class='bb_container escroll'>
				<table class='goods_type_tbl clear_table'>";
				$slide		.= "<tr>";
				$slide		.= $option_data['is_picto']	? "<th></th>" : "";
				$slide		.= $option_data['is_owner'] ? "<th>".__("Batch's owner", ERMAK_BOOKER)."</th>" : "";
				$slide		.= $option_data['is_disl'] 	? "<th>".__("Batch's location", ERMAK_BOOKER)."</th>" : "";
				$slide		.= $option_data['is_sale'] 	? "<th>".__("Sale", ERMAK_BOOKER)."</th>" : "";
				$slide		.= "</tr>";
				
				foreach($cb as $goods_batch)
				{;
					$slide	.= static::get_smp_goods_batch_form($goods_batch->ID, null, Ermak_Booker_Plugin::get_instance()->options['gb_setting']);				
					$i++;
				}
				if($i==0)
					$slide	.= "<tr><td><div class='smc_comment'>".__("No Goods of this type", "smp")."</div></td></tr>";
				$slide		.= "</table>";
				$slide	.= "</div>";
				$slide	.= "</div>";
				$arr[]		= array("title" => $title, "slide" => $slide);
			}	
			$html			.= Assistants::get_switcher($arr, $name, array("class"=>"smc_compact", "slide"=>$slide_id));
			return $html;
		}
		
		static function get_location_store_form($location_id=0, $name="store", $slide_id=0)
		{
			global $user_iface_color, $current_goods_type;
			$all_goods_batchses		= Goods_Batch::get_all_batches(array("store"=>1, "dislocation_id"=>$location_id), false);
			$all_goods_types		= Goods_Type::get_global();
			$sorted_batches			= array();
			foreach($all_goods_types as $goods_type) 
			{
				$sorted_batches[(string)$goods_type->ID]= array();
			}
			
			$arr		= array();
			foreach($all_goods_types as $current_goods_type)
			{
				$sorted_batches[(string)$current_goods_type->ID]	= array_filter($all_goods_batchses, "by_type");
				//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
				//var_dump($sorted_batches[(string)$current_goods_type->ID]);
				
			}
			$option_data	= Ermak_Booker_Plugin::get_instance()->options['store_setting'];				
			$i=0;
			foreach($all_goods_types as $goods_type)
			{
				$title		=  Goods_Type::get_picto($goods_type->ID, 42, -1, count($sorted_batches[(string)$goods_type->ID]) );
				$slide		= "<div>";
				$cb			= $sorted_batches[(string)$goods_type->ID];
				$i = 0;
				$slide		.= '<h3><span style=\'font-weight:700; color:#AAA!important\'>'. __("Goods type", "smp").'</span> ' . $goods_type->post_title.'</h3>' ;
				$slide		.= "
				<div class='bb_container escroll'>
				<table class='goods_type_tbl clear_table'>";
				$slide		.= "<tr>";
				$slide		.= $option_data['is_picto']	? "<th></th>" : "";
				$slide		.= $option_data['is_owner'] ? "<th>".__("Batch's owner", ERMAK_BOOKER)."</th>" : "";
				$slide		.= $option_data['is_disl'] 	? "<th>".__("Batch's location", ERMAK_BOOKER)."</th>" : "";
				$slide		.= $option_data['is_sale'] 	? "<th>".__("Sale", ERMAK_BOOKER)."</th>" : "";
				$slide		.= "</tr>";
				foreach($cb as $goods_batch)
				{
					$slide	.= static::get_smp_goods_batch_form($goods_batch->ID, 'stre', Ermak_Booker_Plugin::get_instance()->options['store_setting']);				
					$i++;
				}
				if($i==0)
					$slide	.= "<tr><td><div class='smc_comment'>".__("No Goods of this type", "smp")."</div></td></tr>";
				$slide		.= "</table>";
				$slide		.= "</div>";
				$slide		.= "</div>";
				$arr[]		= array("title" => $title, "slide" => $slide);
			}	
			$html			.= Assistants::get_switcher($arr, $name, array("class" => "smc_compact", "slide"=>$slide_id));
			return $html;
		}
		static function get_factory_row($id, $params=-1)
		{
			if(!is_array($params))	$params = array('style'=>'padding:2px;');
			$factory		= Factory::get_factory($id);
			$goods_type_id	= $factory->get_goods_type();
			$av				= $factory->get_availbles();
			$owner_id		= $factory->get_owner_id();
			$dislocation_id	= $factory->get_dislocation_id();
			$is_owner		= $factory->is_user_owner();
			$arr			= array();
			$i				= 0;
			$curpr			= 0;
			$max			= 1;
			foreach($av as $a)
			{
				$gt			= Goods_Type::get_instance($a);
				if($a == $goods_type_id)	$curpr = $i;
				$slide		= "<span style='text-transform:uppercase; font-size:12px;'>".$gt->post_title."</span><br><span>" . __("Owner", "smc") . ": <b>" . SMC_Location::get_instance($owner_id)->name. "</b>";
				$arr[]		= array("title" => Goods_Type::get_picto( $a, 40, -1, $factory->get_productivity("", true, $a) ), "slide" => $slide, "exec"=>"eb_chng_fctgt", "args"=>$id.",".$a);
				$i++;
			}
			$batchs			= $factory->get_needs();
			$btc_text		= "<div style='position:relative; display:inline-block; opacity:0; width:340px'>";
			$btc_text		.= is_wp_error($batchs) ? Assistants::echo_me($batchs->get_error_message()) : "<p class='smc_comment'>".__("All available", 'smp')."</p>";			
			$btc_text		.= "</div>";
			switch(true)
			{
				case !is_user_logged_in():
					$change_prod	= "log in please";
					break;
				case $is_owner == true:
					$change_prod	=  Assistants::get_numeric( $factory->get_productivity("", true), array("id"=>"fact_".$id, "name"=> "eb_chnge_fpr", "arg1"=>$id, 'arg2'=>'eb_chnge_fpr'), $factory->get_max_productivity() );
					break;
				case $is_owner == false:
					$change_prod	= __("Owner", "smc"). ": <b>". SMC_Location::get_instance($owner_id)->name . "</b>";
					break;
			}
			return "
			<tr class='".$params['class']."' style='".$params['style']."; width:50%;' fid='$id'>
				<td style='width:auto;' rowspan='2'>".
					Assistants::get_switcher($arr, "av" . $id, array("class" => "smc_compact", "slide"=>$curpr, "name"=>" fid='$id' name='eb_chngfctgtid' ") ).
				"</td>
				<td>
					$change_prod
				</td>
			</tr>
			<tr class='".$params['class']."; width:50%;' fid='$id'>
				<td>
					<center style='position:relative; display:inline-block;'> 
							<span class='description'>".
								__("It will be produced", "smp").
						"	</span>
						<span class='ardn_button' id='factory_over_$id' clid='$id'>
							<i class='fa fa-chevron-down'></i>
						</span>
					</center>
				</td>
			</tr>
			<tr fid='$id'>
				<td colspan='13'>
					<div class='eb_fc_over' id='fc_over$id'>
						$btc_text
					</div>
				</td>
			</tr>";
		}
		static function get_factory_list($meta=-1, $only_mine=true, $name="my_fctrs", $slide_id=0)
		{
			$all_goods_types	= Goods_Type::get_global();
			$factories			= Factories::get_factories($meta);
			if(count($factories)==0)
				return "<div class='smc_comment'>".__("You haven't one Factory. Call Master.", "smp")."</div>";
			$i					= 0;
			$html				= "<div class='eb_mobcont'>
			<table  class='goods_type_tbl clear_table'>";
			foreach($factories as $factory)
			{
				$html			.= self::get_factory_row($factory, array("class"=> $i++%2==0 ? "dark_row" : "light_row" , "style"=>"padding:30px;"));
			}
			$html				.= "</table>
			</div>";
			return $html;
		}
	}
?>